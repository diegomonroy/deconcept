<?php
/**
 * Single Product Sale Flash
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/sale-flash.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

?>
<?php if ( $product->is_on_sale() ) : ?>

	<?php /*echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'woocommerce' ) . '</span>', $post, $product );*/ ?>
	<!-- Begin Custom AA -->
	<?php
	$discount = 'DESCUENTO';
	$category_1_id = array( 18 );
	$category_1 = array( 'tapetes' );
	if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
		$discount = 'DESCUENTO';
	}
	$category_1_id = array( 67 );
	$category_1 = array( 'pisos' );
	if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
		$discount = 'DESCUENTO';
	}
	?>
	<?php /*echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale"><span class="text">' . $discount . '</span></span>', $post, $product );*/ ?>
	<span class="onsale"><span class="text"><?php the_field( 'texto_descuento' ); ?></span></span>
	<!-- End Custom AA -->

<?php endif;

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
