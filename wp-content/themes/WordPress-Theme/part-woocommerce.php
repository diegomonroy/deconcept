<?php
$left = 'left';
$column = '';
$banner = '';
$banner_small = '';
if ( ! is_product() ) {
	$column = 'medium-9';
}
/* Tapetes */
$category_1_id = 18;
$category_1 = 'tapetes';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_tapetes';
		$column = 'medium-9';
		$banner = 'banner_tapetes';
		$banner_small = 'banner_small_tapetes';
	}
}
/* Alfombras */
$category_1_id = 63;
$category_1 = 'alfombras';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_alfombras';
		$column = 'medium-9';
		$banner = 'banner_alfombras';
		$banner_small = 'banner_small_alfombras';
	}
}
/* Pisos */
$category_1_id = 67;
$category_1 = 'pisos';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_pisos';
		$column = 'medium-9';
		$banner = 'banner_pisos';
		$banner_small = 'banner_small_pisos';
	}
}
/* Atrapamugre & Grama */
$category_1_id = 74;
$category_1 = 'atrapamugre-grama';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_atrapamugre_y_grama';
		$column = 'medium-9';
		$banner = 'banner_atrapamugre_y_grama';
		$banner_small = 'banner_small_atrapamugre_y_grama';
	}
}
/* Cortinas */
$category_1_id = 83;
$category_1 = 'cortinas';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_cortinas';
		$column = 'medium-9';
		$banner = 'banner_cortinas';
		$banner_small = 'banner_small_cortinas';
	}
}
/* Kronowall 3D */
$category_1_id = 89;
$category_1 = 'kronowall-3d';
if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
	if ( ! is_product() ) {
		$left = 'left_kronowall_3d';
		$column = 'medium-9';
		$banner = 'banner_kronowall_3d';
		$banner_small = 'banner_small_kronowall_3d';
	}
}
/* Buscar */
if ( is_search() && $post_type == 'product' ) {
	$left = 'left_buscar';
	$column = 'medium-9';
	$banner = 'banner_buscar';
	$banner_small = 'banner_small_buscar';
}
?>
<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( $banner ); ?>
			</div>
		</div>
	</section>
<!-- End Banner -->
<!-- Begin Content -->
	<section class="content" data-wow-delay="0.5s">
		<div class="row expanded">
			<?php if ( ! is_product() ) : ?>
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( $left ); ?>
			</div>
			<?php endif; ?>
			<div class="small-12 <?php echo $column; ?> columns">
				<!-- Begin Banner Small -->
					<section class="banner_small" data-wow-delay="0.5s">
						<div class="row">
							<div class="small-12 columns">
								<?php dynamic_sidebar( $banner_small ); ?>
							</div>
						</div>
					</section>
				<!-- End Banner Small -->
				<div class="woocommerce">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->