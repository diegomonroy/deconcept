<!-- Begin Top -->
	<header class="nav-down">
		<section class="top_1" data-wow-delay="0.5s">
			<div class="row collapse align-center align-middle">
				<div class="small-12 columns">
					<?php dynamic_sidebar( 'top_1' ); ?>
				</div>
			</div>
		</section>
		<section class="top" data-wow-delay="0.5s">
			<div class="row collapse align-center align-middle">
				<div class="small-12 medium-2 columns text-center">
					<?php dynamic_sidebar( 'logo' ); ?>
				</div>
				<div class="small-12 medium-7 columns">
					<?php dynamic_sidebar( 'menu' ); ?>
				</div>
				<div class="small-12 medium-3 columns text-center">
					<?php dynamic_sidebar( 'search_cart_language' ); ?>
					<?php dynamic_sidebar( 'search_top' ); ?>
				</div>
			</div>
		</section>
	</header>
<!-- End Top -->