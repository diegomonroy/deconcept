// JavaScript Document

/* ************************************************************************************************************************

WordPress Theme

File:			app.js
Author:			Amapolazul
Homepage:		www.amapolazul.com
Copyright:		2018

************************************************************************************************************************ */

/* Foundation */

//$(document).foundation();

/* WOW */

new WOW().init();

/* jQuery */

jQuery.noConflict();

jQuery(document).ready(function ($) {
	jQuery(document).foundation();
});

jQuery(document).ready(function () {
	/* Menu */
	jQuery( 'ul.sub-menu' ).addClass( 'menu' );
	/* Cart */
	jQuery( 'button.woosq-btn' ).html( 'Compra Rápida' );
	/* Newsletter */
	jQuery( '.tnp-email' ).attr( 'placeholder', 'insertar correo aquí' );
	jQuery( 'input.tnp-submit' ).prop( 'value', 'ENVIAR' );
	/* Search */
	jQuery( '.moduletable_to4' ).hide();
	jQuery( '.moduletable_to4' ).append( '<a href="#" id="close-search" class="button close">Cerrar</a>' );
	jQuery( 'a#open-search' ).click(function () {
		jQuery( '.moduletable_to4' ).slideDown( 1000 );
		return false;
	});
	jQuery( 'a#close-search' ).click(function () {
		jQuery( '.moduletable_to4' ).slideUp( 1000 );
		return false;
	});
/* OWL */
/*jQuery( '.flex-control-nav' ).owlCarousel({
items: 1,
loop: true,
autoplayHoverPause: true,
animateOut: 'slideOutUp',
animateIn: 'slideInUp'
});*/
});

var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = jQuery( 'header' ).outerHeight();

jQuery(window).scroll(function ( event ) {
	didScroll = true;
});

setInterval(function () {
	if ( didScroll ) {
		hasScrolled();
		didScroll = false;
	}
}, 250);

function hasScrolled() {
	var st = jQuery( this ).scrollTop();
	if ( Math.abs( lastScrollTop - st ) <= delta )
		return;
	if ( st > lastScrollTop && st > navbarHeight ) {
		jQuery( 'header' ).removeClass( 'nav-down' ).addClass( 'nav-up' );
	} else {
		if ( st + jQuery(window).height() < jQuery(document).height() ) {
			jQuery( 'header' ).removeClass( 'nav-up' ).addClass( 'nav-down' );
		}
	}
	lastScrollTop = st;
}