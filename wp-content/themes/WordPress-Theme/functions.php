<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/build/foundation/css/foundation.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/font-awesome/css/font-awesome.min.css', false );
	wp_enqueue_style( 'owl-carousel-css-1', get_template_directory_uri() . '/build/owl.carousel/dist/assets/owl.carousel.min.css', false );
	wp_enqueue_style( 'owl-carousel-css-2', get_template_directory_uri() . '/build/owl.carousel/dist/assets/owl.theme.default.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/foundation/js/vendor/jquery.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/foundation/js/vendor/what-input.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/foundation/js/vendor/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/fancybox/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/wow/wow.min.js', false );
	wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/build/owl.carousel/dist/owl.carousel.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'cart-menu' => __( 'Cart Menu' ),
			'main-menu' => __( 'Main Menu' ),
			'product-menu' => __( 'Product Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to add theme support
 */
add_theme_support( 'post-thumbnails' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Top 1',
			'id' => 'top_1',
			'before_widget' => '<div class="moduletable_to11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Menu',
			'id' => 'menu',
			'before_widget' => '<div class="moduletable_to2">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Cart Language',
			'id' => 'search_cart_language',
			'before_widget' => '<div class="moduletable_to3">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Search Top',
			'id' => 'search_top',
			'before_widget' => '<div class="moduletable_to4 text-right">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner - Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner - Tapetes',
			'id' => 'banner_tapetes',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner - Alfombras',
			'id' => 'banner_alfombras',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner - Pisos',
			'id' => 'banner_pisos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner - Atrapamugre & Grama',
			'id' => 'banner_atrapamugre_y_grama',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner - Cortinas',
			'id' => 'banner_cortinas',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner - Kronowall 3D',
			'id' => 'banner_kronowall_3d',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner - Buscar',
			'id' => 'banner_buscar',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Small - Tapetes',
			'id' => 'banner_small_tapetes',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Small - Alfombras',
			'id' => 'banner_small_alfombras',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Small - Pisos',
			'id' => 'banner_small_pisos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Small - Atrapamugre & Grama',
			'id' => 'banner_small_atrapamugre_y_grama',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Small - Cortinas',
			'id' => 'banner_small_cortinas',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Small - Kronowall 3D',
			'id' => 'banner_small_kronowall_3d',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Small - Buscar',
			'id' => 'banner_small_buscar',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left',
			'id' => 'left',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left - Tapetes',
			'id' => 'left_tapetes',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left - Alfombras',
			'id' => 'left_alfombras',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left - Pisos',
			'id' => 'left_pisos',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left - Atrapamugre & Grama',
			'id' => 'left_atrapamugre_y_grama',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left - Cortinas',
			'id' => 'left_cortinas',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left - Kronowall 3D',
			'id' => 'left_kronowall_3d',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Left - Buscar',
			'id' => 'left_buscar',
			'before_widget' => '<div class="moduletable_le1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 1 - Categorías',
			'id' => 'block_1',
			'before_widget' => '<div class="moduletable_b11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 2 - Productos',
			'id' => 'block_2',
			'before_widget' => '<div class="moduletable_b21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 3 - Blog',
			'id' => 'block_3',
			'before_widget' => '<div class="moduletable_b31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 4 - Quiénes Somos',
			'id' => 'block_4',
			'before_widget' => '<div class="moduletable_b41">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 5 - Instagram',
			'id' => 'block_5',
			'before_widget' => '<div class="moduletable_b51">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Block 6',
			'id' => 'block_6',
			'before_widget' => '<div class="moduletable_b61">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Function to declare WooCommerce support
 */
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'woocommerce_support' );

/*
 * Functions to declare WooCommerce image sizes
 */
function woocommerce_catalog_image( $size ) {
	return array(
		'width' => 500,
		'height' => 500,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_thumbnail', 'woocommerce_catalog_image' );

function woocommerce_single_image( $size ) {
	return array(
		'width' => 600,
		'height' => 600,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_single', 'woocommerce_single_image' );

function woocommerce_gallery_image( $size ) {
	return array(
		'width' => 200,
		'height' => 200,
		'crop' => 1,
	);
}
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', 'woocommerce_gallery_image' );

/*
 * Function to declare WooCommerce products per page
 */
function new_loop_shop_per_page( $cols ) {
	$cols = 12;
	return $cols;
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

/*
 * Function to declare WooCommerce products per column
 */
if ( ! function_exists( 'loop_columns' ) ) {
	function loop_columns() {
		$number = 3;
		/* Atrapamugre & Grama */
		/*$category_1_id = 74;
		$category_1 = 'atrapamugre-grama';
		if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
			$number = 4;
		}*/
		/* Cortinas */
		/*$category_1_id = 83;
		$category_1 = 'cortinas';
		if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
			$number = 4;
		}*/
		/* Kronowall 3D */
		/*$category_1_id = 89;
		$category_1 = 'kronowall-3d';
		if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
			$number = 4;
		}*/
		return $number;
	}
}
add_filter( 'loop_shop_columns', 'loop_columns' );

/*
 * Function to remove WooCommerce price
 */
function remove_price( $price, $product ) {
	/*$hide_for_categories = array( 'alfombras', 'pisos', 'atrapamugre-grama', 'cortinas', 'kronowall-3d' );*/
	$hide_for_categories = array( 'alfombras', 'vinilo-rollo', 'lvt-sistema-pegado', 'spc', 'atrapamugre-grama', 'cortinas', 'kronowall-3d' );
	if ( has_term( $hide_for_categories, 'product_cat', $product->get_id() ) ) {
		$price = '';
	}
	return $price;
}
add_filter( 'woocommerce_variable_sale_price_html', 'remove_price', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'remove_price', 10, 2 );
add_filter( 'woocommerce_get_price_html', 'remove_price', 10, 2 );

function custom_remove_html() {
	$html = '';
	$category_1_id = 63;
	$category_1 = 'alfombras';
	if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
		$html = '<style>.content .woocommerce ul.products button.woosq-btn, .block_2 .moduletable_b21 button.woosq-btn { display: none !important; }</style>';
	}
	/*$category_1_id = 67;*/
	/*$category_1 = 'pisos';*/
	$category_1_id = array( 70, 72, 73 );
	$category_1 = array( 'vinilo-rollo', 'lvt-sistema-pegado', 'spc' );
	if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
		$html = '<style>.content .woocommerce ul.products button.woosq-btn, .block_2 .moduletable_b21 button.woosq-btn { display: none !important; }</style>';
	}
	$category_1_id = 74;
	$category_1 = 'atrapamugre-grama';
	if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
		$html = '<style>.content .woocommerce ul.products button.woosq-btn, .block_2 .moduletable_b21 button.woosq-btn { display: none !important; }</style>';
	}
	$category_1_id = 83;
	$category_1 = 'cortinas';
	if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
		$html = '<style>.content .woocommerce ul.products button.woosq-btn, .block_2 .moduletable_b21 button.woosq-btn { display: none !important; }</style>';
	}
	$category_1_id = 89;
	$category_1 = 'kronowall-3d';
	if ( is_product_category( $category_1 ) || has_term( $category_1, 'product_cat' ) || term_is_ancestor_of( $category_1_id, get_queried_object()->term_id, 'product_cat' ) ) {
		$html = '<style>.content .woocommerce ul.products button.woosq-btn, .block_2 .moduletable_b21 button.woosq-btn { display: none !important; }</style>';
	}
	echo $html;
}
add_action( 'woocommerce_after_shop_loop_item', 'custom_remove_html', 10 );

function disable_cars_initially() {
	global $product;
	/*$hide_for_categories = array( 'alfombras', 'pisos', 'atrapamugre-grama', 'cortinas', 'kronowall-3d' );*/
	$hide_for_categories = array( 'alfombras', 'vinilo-rollo', 'lvt-sistema-pegado', 'spc', 'atrapamugre-grama', 'cortinas', 'kronowall-3d' );
	if ( has_term( $hide_for_categories, 'product_cat', $product->id ) ) {
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
		remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
	}
}
add_action('wp','disable_cars_initially');

/*
 * Custom hooks in WooCommerce
 */
function custom_description() {
	/*the_content();*/
}
add_action( 'woocommerce_single_product_summary', 'custom_description', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

// remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function custom_clear() {
	global $product;
	$web = get_site_url() . '/';
	$shipping = '';
	$hide_for_categories = array( 'laminados', 'laminado-resistente-a-la-humedad', 'outlet' );
	if ( has_term( $hide_for_categories, 'product_cat', $product->id ) ) {} else {
		$shipping = '
			<div class="row expanded align-center align-middle">
				<div class="small-12 medium-3 columns">
					<div class="row align-center align-middle">
						<div class="small-12 medium-4 columns">
							<p class="text-center"><img src="' . $web . 'wp-content/uploads/Icon-Clock.png" alt=""></p>
						</div>
						<div class="small-12 medium-8 columns">
							<h4>Tiempos de Entrega</h4>
							<p class="text-justify">De 3 a 5 días hábiles</p>
						</div>
					</div>
				</div>
				<!--<div class="small-12 medium-6 columns lines">
					<div class="row align-center align-middle">
						<div class="small-12 medium-4 columns">
							<p class="text-center"><img src="' . $web . 'wp-content/uploads/Icon-Truck.png" alt=""></p>
						</div>
						<div class="small-12 medium-8 columns">
							<h4>Envío Gratis</h4>
							<p class="text-justify">Para compras superiores a $ 390.000.</p>
							<p class="text-justify">Compras inferiores al monto mencionado el cliente asume los costos de ﬂete.</p>
						</div>
					</div>
				</div>-->
				<div class="small-12 medium-3 columns">
					<div class="row align-center align-middle">
						<div class="small-12 medium-4 columns">
							<p class="text-center"><img src="' . $web . 'wp-content/uploads/Icon-Location.png" alt=""></p>
						</div>
						<div class="small-12 medium-8 columns">
							<h4>Cobertura</h4>
							<p class="text-justify">Envíos a toda Colombia excepto San Andres.</p>
						</div>
					</div>
				</div>
			</div>
		';
	}
	$tab_2 = '';
	$tab_2_content = '';
	if ( get_field( 'fibra' ) ) :
		$tab_2 = '<li class="tabs-title"><a data-tabs-target="panel2" href="#panel2">Fibra</a></li>';
		$tab_2_content = '<div class="tabs-panel" id="panel2">' . get_field( 'fibra' ) . '</div>';
	endif;
	$tab_3 = '';
	$tab_3_content = '';
	if ( get_field( 'garantia' ) ) :
		$tab_3 = '<li class="tabs-title"><a data-tabs-target="panel3" href="#panel3">Garantía</a></li>';
		$tab_3_content = '<div class="tabs-panel" id="panel3">' . get_field( 'garantia' ) . '</div>';
	endif;
	$tab_4 = '';
	$tab_4_content = '';
	if ( get_field( 'cuidado_y_limpieza' ) ) :
		$tab_4 = '<li class="tabs-title"><a data-tabs-target="panel4" href="#panel4">Cuidado y Limpieza</a></li>';
		$tab_4_content = '<div class="tabs-panel" id="panel4">' . get_field( 'cuidado_y_limpieza' ) . '</div>';
	endif;
	$tab_5 = '';
	$tab_5_content = '';
	if ( get_field( 'trabajos_realizados' ) ) :
		$tab_5 = '<li class="tabs-title"><a data-tabs-target="panel5" href="#panel5">Trabajos Realizados</a></li>';
		$tab_5_content = '<div class="tabs-panel" id="panel5">' . get_field( 'trabajos_realizados' ) . '</div>';
	endif;
	echo '
		<div class="clear"></div>
		' . $shipping . '
		<ul class="tabs menu align-center" data-tabs id="product-tabs">
			<li class="tabs-title is-active"><a href="#panel1" aria-selected="true">Descripción</a></li>
			' . $tab_2 . '
			' . $tab_3 . '
			' . $tab_4 . '
			' . $tab_5 . '
		</ul>
		<div class="tabs-content" data-tabs-content="product-tabs">
			<div class="tabs-panel is-active" id="panel1">
	';
	the_content();
	echo '
			</div>
			' . $tab_2_content . '
			' . $tab_3_content . '
			' . $tab_4_content . '
			' . $tab_5_content . '
		</div>
		<p class="text-center"><img src="' . get_field( 'slider' ) . '"></p>
	';
}
add_action( 'woocommerce_after_single_product_summary', 'custom_clear', 10 );
 
function bbloomer_change_gallery_columns() {
     return 1; 
}
add_filter ( 'woocommerce_product_thumbnails_columns', 'bbloomer_change_gallery_columns' );