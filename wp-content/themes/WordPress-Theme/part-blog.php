<!-- Begin Content -->
	<section class="content blog" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns category_main">
				<div class="row collapse">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div class="small-12 medium-4 columns">
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								<div class="row align-center align-middle">
									<div class="small-12 columns">
										<p class="text-center"><a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?></a></p>
									</div>
									<!--<div class="small-12 medium-9 columns">
										<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
										<?php the_excerpt(); ?>
										<p class="text-right"><a href="<?php the_permalink(); ?>" class="hollow button">Leer más...</a></p>
									</div>-->
								</div>
							</article>
						</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->